const mongoose = require('mongoose');
const slugify = require('slugify');
const { marked } = require('marked');
const dompurify = require('dompurify');
const { JSDOM } = require('jsdom');
const purified_dom = dompurify(new JSDOM().window); // sanitizes html

const article_schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    markdown: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    slug: {
        type: String,
        required: true,
        unique: true
    },
    sanitizedHtml: {
        type: String,
        required: false
    }
});
article_schema.pre('validate', function (next) {
    if (this.title) {
        this.slug = slugify(this.title, { lower: true, strict: true });
    }
    if (this.markdown) {
        console.log("hiii1");
        console.log(this.slug);
        this.sanitizedHtml = purified_dom.sanitize(marked.parse(this.markdown));
        console.log("hii");
    }
    next();
})
module.exports = mongoose.model('article', article_schema);