require('dotenv');
const PORT = process.env.PORT || 6969;
const xpress = require('express');
const app = xpress();
const article_router = require('./routes/articles');
const article_model = require('./models/articles');
const mongoose = require('mongoose');
const method_override = require('method-override');
mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost/mdb_db');

app.set('view engine', 'ejs');
app.use(xpress.urlencoded({ extended: false }));
app.use(method_override('_override_meth'));


app.use('/articles', article_router);
// const articles = article_model.find().sort();
app.get('/', async (req, res) => {
    const all_articles = await article_model.find().sort({ created_at: 'desc' });
    res.render('articles/index', { articles: all_articles });
});
// app.get('/', (req, res) => (res.send('chal rha')));
// app.get('/', (req, res) => res.json({ msg: 'gg' }));
app.listen(PORT);
