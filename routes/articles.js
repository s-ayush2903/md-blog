const xpress = require('express');
const { append, render } = require('express/lib/response');
const router = xpress.Router();
const Article = require('../models/articles');

router.get('/', (req, res) => res.send('inside articles section'));

router.get('/new', (req, res) => res.render('articles/new_article', { article: new Article() }))

router.get('/:slug', async (req, res) => {
    const _found_article = await Article.findOne({ slug: req.params.slug }); // imp await
    // const _found_article = await Article.findById(req.params.id); // imp await
    if (_found_article == null) {
        res.redirect('/');
    } else {
        res.render('articles/read', { article: _found_article });
    }
});

router.get('/edit/:id', async (req, res) => {
    const _article = await Article.findById(req.params.id);
    if (_article) {
        res.render('articles/edit', { article: _article });
    } else {
        res.status(404).json({ msg: 'requested article does not exist' });
    }
})

router.put('/:id', async (req, res, next) => {
    req.article = await Article.findById(req.params.id);
    next();
}, save_and_redirect('edit'));

router.post('/', async (req, res, next) => {
    req.article = new Article;
    next();
}, save_and_redirect('new'));

router.delete('/:id', async (req, res) => {
    await Article.findByIdAndDelete(req.params.id);
    res.redirect('/');
})

function save_and_redirect(path) {
    return async (req, res) => {
        let _article = req.article;
            _article.title = req.body.title,
            _article.description = req.body.description,
            _article.markdown = req.body.markdown
        try {
            _article = await _article.save();
            res.redirect(`/articles/${_article.slug}`);
        } catch (e) {
            res.render(`articles/${path}`, { article: _article });
        }
    }
}

module.exports = router;